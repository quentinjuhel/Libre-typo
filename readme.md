# FLOSS fonts

(fr)Typo libre est une liste de fonderie et de ressources autour de la typographie libre et open-source.
N'hésitez pas à participer !

(en)Typo libre is a list of foundries and resources of libre and open-source typeface.
Don't hesitate to participate!

## Founderies

Velvetyne : https://www.velvetyne.fr 

Collletttivo : http://collletttivo.it/

Warsaw type : https://kroje.org/en/

Impallari Type : https://github.com/impallari?tab=repositories

Hanken : https://hanken.co

Fonderie downlaod : https://www.fonderie.download/

Love letters : http://www.love-letters.be/

OSP foundry : http://osp.kitchen/foundry/

Cat fonts : http://www.peter-wiegel.de/

republi.sh : [www.republi.sh](http://www.republi.sh)

Degheest-type : http://www.degheest-types.com/

Bonjour Monde () : https://gitlab.com/bonjour-monde/fonderie

Jens Kutilek : https://www.kutilek.de/

Lobby24 :  https://lobbby24.com/About

Manufactura Indepedente : http://manufacturaindependente.org/

Quarantine fonts (Jens Kutilek) : https://github.com/jenskutilek/quarantine-fonts

Tunera Type Foundry : http://www.tunera.xyz/

Laïc Type : https://laic.pl/free-fonts

Fond de riz : https://fonderiz.fr/

## Fonts

Écrou, by Guillaume Santial : https://gstl.fr/ecrou/ (license Creative Commons CC BY-SA)

Cal Sans, by Mark Davis for Cal.com : https://github.com/calcom/font/releases/tag/v1.0.0 

Blocksync, Eli Heuer : https://github.com/eliheuer/blocksync
### Not open source but free	

~~Orlando Furioso Type Club : https://orlandofurioso.xyz/~~

~~Proto Type : https://proto-type.se/type~~

~~Lars studio: https://lars.studio/typefaces.html~~	

~~https://sdfggvfvj.github.io/notyourtype/test/index.html~~

## Schools and workshop

La fonderie de l'ERG en croissance : http://chasse-ouverte.erg.be

ANRT : https://t.co/b9D8Z4F8Sp?amp=1

le 75 : http://typotheque.le75.be/

SUVA Type Foundry : https://www.suvatypefoundry.ee/#footer

Lycée Jacques Prévert, Boulogne-Billancourt (DNMADE, Workshop Lucas Descroix)
http://lucasdescroix.fr/teaching/gottacatch/index.html

Esad Type : http://postdiplome.esad-amiens.fr/

## Collections

Badass Libre Fonts by Womxn : https://www.design-research.be/by-womxn

la typothèque de Luuse : http://typotheque.luuse.io/

Use and modify de Raphaël Bastide : https://usemodify.com 
(ofont)

Fontain par Lakkon : https://fontain.org/

Codeface : https://github.com/chrissimpkins/codeface

Font Squirrel : [https://www.fontsquirrel.com/fonts/list/find_fonts?filter%5Blicense%5D%5B0%5D=open](https://www.fontsquirrel.com/fonts/list/find_fonts?filter[license][0]=open)

Open Foundry : https://open-foundry.com

The league of moveable type : https://www.theleagueofmoveabletype.com/

Font Library : https://fontlibrary.org

Goofle fonts : https://fonts.google.com

Uncut : https://www.uncut.wtf/

## Specimen

Inter : https://rsms.me/inter/

karrik : http://karrik.phantom-foundry.com/#third
karrik game : http://www.karrik-game.velvetyne.fr/

Health the web : http://healtheweb.site/

## Libre font from retail foundry

Synco (altiplano foundry) : 
https://www.altiplano.xyz/assets/Uploads/SYNCO-2020.zip
https://gitlab.com/arielmartinperez/synco-cyrillic

Ilisarniq (by Copper and Brasses with Iknut glyph) : https://coppersandbrasses.com/typefaces/ilisarniq/

## Tools

FontForge : https://fontforge.org/en-US/

Modulo FOnt : https://f-nt.eu/fr

Glyphr : https://www.glyphrstudio.com/

Birdfont : https://www.glyphrstudio.com/blog/

Brutalita : https://brutalita.com/

## Ressources : 

Raphaël Bastide references : https://gitlab.com/raphaelbastide/libre-foundries

Jérémy Landes : https://gitlab.com/StudioTriple/vite_et_mal/-/blob/master/documentation/type%20design%20education%20ressources.md
